package com.ruoyi.system.service;

import java.util.Date;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.mapper.SysUserMapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class SystemListService {
	@Autowired
	SysRoleMapper sysRoleMapper;

	@Autowired
	SysDeptMapper sysDeptMapper;
	
	@Autowired
	SysUserMapper sysUserMapper;
	
	

    @DataScope(deptAlias = "d")
    public PageQuery<SysUser> selectUserList(BaseEntity baseEntity , Map<String,Object> params){
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		params.put("params", baseEntity.getParams());
        return sysUserMapper.queryByCondition(sysUserMapper.createPageQuery(params, ServletUtils.getParameterToInt(Constants.PAGE_NUM), ServletUtils.getParameterToInt(Constants.PAGE_SIZE)));
    }
	
	
    @DataScope(deptAlias = "d")
    public PageQuery<SysRole> selectRoleList(BaseEntity baseEntity , Map<String,Object> params){
    	PageQuery<SysRole> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		params.put("params", baseEntity.getParams());
		pageQuery.setParas(params);
        return sysRoleMapper.queryByCondition(pageQuery);
    }

    @DataScope(deptAlias = "d", userAlias = "u")
	public PageQuery<?> selectAuthUserList(BaseEntity baseEntity, Map<String, Object> params) {
		PageQuery<SysUser> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		params.put("params", baseEntity.getParams());
		pageQuery.setParas(params);
        return sysUserMapper.queryAllocatedByCondition(pageQuery);
	}

    @DataScope(deptAlias = "d", userAlias = "u")
	public PageQuery<?> selectUnauthUserList(BaseEntity baseEntity, Map<String, Object> params) {

		PageQuery<SysUser> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		params.put("params", baseEntity.getParams());
		pageQuery.setParas(params);
        return sysUserMapper.queryUnallocatedByCondition(pageQuery);
	}
    
    
}
